<?php

namespace Components\Forms;

use Components\Forms\Actions\locationsForm;

interface ILocations
{
    /**
     * @param null $location
     * @return locationsForm
     */
    public function create($location = NULL);
}