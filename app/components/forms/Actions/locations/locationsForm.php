<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 27.5.2016
 * Time: 19:08
 */

namespace Components\Forms\Actions;

use App\Models\Services\locationService;
use Components\Forms\Core\BaseFormControl;
use Components\Forms\Core\IBaseForm;
use Components\Visuals\IGrid;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\Http\Request;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

class locationsForm extends BaseFormControl
{
    /** @var array */
    public $onSave = [];

    /** @var IBaseForm $form */
    private $form;

    /** @var User $user */
    private $user;

    /** @var IGrid $grid */
    private $grid;

    /** @var Request $httpRequest */
    private $httpRequest;

    /** @var locationService $locationService */
    private $locationService;

    /** @var null|ActiveRow $location */
    private $location;

    public function __construct(
        IBaseForm $form, 
        User $user, 
        IGrid $grid, 
        Request $httpRequest,
        locationService $locationService,
        $location = NULL)
    {
        parent::__construct();
        $this->form             = $form;
        $this->user             = $user;
        $this->location         = $location;
        $this->httpRequest      = $httpRequest;
        $this->locationService  = $locationService;
        $this->location         = $location;
        $this->grid             = $grid;
    }

    public function render()
    {
        $this->template->setFile(__DIR__ . '\default.latte');
        $this->template->render();
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);
        $location   = $this->location;
        $grid       = $this->grid;
        if($location){
            $form = $this->getComponent('form');
            $form->setValues($location);

            $seats                  = $this->locationService->getSeats($location);
            $instanceData           = new ArrayHash();
            $instanceData->cols     = $location->cols;
            $instanceData->rows     = $location->rows;
            $instanceData->bookable = $seats;

            $this->grid = $grid->create($instanceData);
        }else{
            $this->grid = $grid->create();
        }
    }

    protected function createComponentForm()
    {
        $form = $this->form->create();
        $form->addHidden('id');
        $form->addText('name', 'Název');
        $form->addText('city', 'Město');
        $form->addText('street', 'Ulice');
        $form->addText('number', 'Číslo popisné');
        $form->addText('cols', 'Sloupce')
            ->setAttribute('class', 'ajax')
            ->setAttribute('id', 'x')
            ->setDefaultValue(1)
            ->setType('number');
        $form->addText('rows', 'Řádky')
            ->setAttribute('class', 'ajax')
            ->setAttribute('id', 'y')
            ->setDefaultValue(1)
            ->setType('number');
        $form->addHidden('bookable')
            ->setRequired()
            ->setAttribute('id', 'selectedCells');

        $form->addSubmit('save', 'save');

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }

    /**
     * Change size of grid
     * TODO: Maybe to be changed from post to param
     */
    public function handleSize()
    {
        if ($this->presenter->isAjax()) {
            $x = $this->httpRequest->post['x'];
            $y = $this->httpRequest->post['y'];
            $this->grid->handleSize($x, $y);
            $this->redrawControl('grid');
        }
    }

    protected function createComponentGrid()
    {
        return $this->grid;
    }

    public function formSuccess(Form $form, ArrayHash $vals)
    {
        $vals->createdBy_id = $this->user->getId();
        $seats              = explode(",", $vals->bookable);

        unset($vals->bookable);

        try{
            $this->locationService->handle($vals, $seats);
        }catch (\Exception $e){
            $form->addError('LAN-Party již existuje');
        }
    }
}