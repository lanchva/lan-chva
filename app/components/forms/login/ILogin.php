<?php

namespace Components\Forms;

interface ILogin
{
    /** @return loginForm */
    public function create();
}