<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 10.5.2016
 * Time: 20:49
 */

namespace Components\Forms;

use Components\Forms\Core\BaseFormControl;
use Components\Forms\Core\IBaseForm;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

class loginForm extends BaseFormControl
{
    public $onSave = [];

    /** @var IBaseForm $form */
    private $form;

    /** @var User $user */
    private $user;

    public function __construct(IBaseForm $form, User $user)
    {
        $this->form = $form;
        $this->user = $user;
    }

    public function render()
    {
        $this->template->setFile(__DIR__ . "/default.latte");
        $this->template->render();
    }

    protected function createComponentForm()
    {
        $form = $this->form->create();
        $form->addText('nick', 'Přezdívka')
            ->setRequired();
        $form->addPassword('password', 'Heslo');
        $form->addSubmit('login', 'Přihlásit');

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }

    public function formSuccess(Form $form, ArrayHash $vals)
    {
        try{
            $this->user->login($vals);;
        }catch(AuthenticationException $e){
            $parent = $this->getPresenter();
            $parent->flashMessage($e, 'warning');
            $parent->redirect('this');
        }

        $this->onSave();
    }
}