<?php

namespace Components\Forms;

interface IRegistration
{
    /** @return registrationForm */
    public function create();
}