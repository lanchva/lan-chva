<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 3.5.2016
 * Time: 19:58
 */

namespace Components\Forms;


use App\Models\Services\userService;
use Components\Forms\Core\BaseFormControl;
use Components\Forms\Core\IBaseForm;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Utils\ArrayHash;

final class registrationForm extends BaseFormControl
{
    /** @var IBaseForm $form */
    private $form;

    /** @var userService $user */
    private $user;

    /**
     * registrationForm constructor.
     * @param IBaseForm $form
     * @param userService $user
     */
    public function __construct(IBaseForm $form, userService $user)
    {
        $this->form = $form;
        $this->user = $user;
    }

    public function render()
    {
        parent::render();
        $this->template->setFile(__DIR__ . "/default.latte");
        $this->template->render();
    }

    /**
     * ToDo: add Datepicker (voda/date-input)
     * @return Core\BaseForm
     */
    protected function createComponentForm()
    {
        $form = $this->form->create();
        $form->addText('nick', 'Nick');
        $form->addText('email', 'Email');
        $form->addText('name', 'Jméno');
        $form->addText('surname', 'Příjmení');

        $form->addPassword('password', 'Heslo');
        $form->addPassword('passwordVerify', 'Heslo znovu')
            ->addRule(Form::EQUAL, 'Hesla se musejí shodovat!', $form['password']);

        $form->addCheckbox('terms', 'Podmínky')
            ->addRule(FORM::EQUAL, 'Musíte souhlasit s podmínky použití', TRUE);
        $form->addSubmit('save', 'Registrovat');

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }

    /**
     * ToDo: Send registration email
     * @param Form $form
     * @param ArrayHash $vals
     */
    public function formSuccess(Form $form, ArrayHash $vals)
    {
        unset($vals->passwordVerify);
        $this->user->add($vals);

        $this->onSave();
    }
}