<?php

namespace Components\Forms\Core;

interface IBaseForm 
{
	/** @return BaseForm */
	public function create();
}
