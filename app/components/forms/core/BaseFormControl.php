<?php

namespace Components\Forms\Core;

class BaseFormControl extends BaseControl
{
	/**
	 * Store functions to be called after successful form submitting
	 * @var array
	 */
	public $onSave = [];

	public function render()
	{
		#echo $this['form'];
	}
}
