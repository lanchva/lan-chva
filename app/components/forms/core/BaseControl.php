<?php

namespace Components\Forms\Core;

use Nette\Application\UI\Control;

class BaseControl extends Control
{

	/**
	 * @param string $message
	 * @param string $type
	 * @return boolean
	 */
	public function flashMessage($message, $type = 'info') 
	{
		$this->getPresenter()->flashMessage($message, $type);
	}
}