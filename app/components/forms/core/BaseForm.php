<?php

namespace Components\Forms\Core;

use Nette\Application\UI\Form;
use Kdyby\Translation\Translator;

class BaseForm extends Form
{
	/** @param Translator $translator */
	public function __construct(Translator $translator) 
	{
		parent::__construct();
		$renderer = $this->getRenderer();

		$this->setTranslator($translator);
		$this->setWrappers($renderer);
	}

	/**
	 * TwitterBootstrap implementation for non-manual rendered factories
	 * @param $renderer
	 * @return mixed
	 */
	private function setWrappers($renderer)
	{		
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['container'] = 'div class=col-sm-9';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.submit'] = 'btn';
		$renderer->wrappers['control']['.file'] = 'form-control';
		
		return $renderer;
	}
}
