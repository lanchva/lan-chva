<?php

namespace Components\Visuals;

use Nette\Utils\ArrayHash;

interface IGrid
{
    /**
     * @param null|ArrayHash $instanceData
     * @return grid
     */
    public function create($instanceData = null);
}