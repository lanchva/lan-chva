<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 31.5.2016
 * Time: 19:19
 */

namespace Components\Visuals;

use Nette\Application\UI\Control;
use Nette\Database\Table\GroupedSelection;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class grid extends Control
{
    /** @var int X max value */
    public $col = 1;

    /** @var int Y max value */
    public $row = 1;

    /** @var null|GroupedSelection $bookable */
    protected $bookable = NULL;

    /** @var ArrayHash Grid structure and content*/
    protected $grid;

    /** @var   */
    protected $instance;

    /**
     * grid constructor.
     * @param null|ArrayHash $instanceData Data of already created grid
     * $instanceData->x = cols
     * $instanceData->y = rows
     * $instanceData->bookable = GroupSelection
     */
    public function __construct($instanceData = NULL)
    {
        parent::__construct();
        $this->instance = $instanceData;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);
        if($this->instance){
            $this->col      = $this->instance->cols;
            $this->row      = $this->instance->rows;
            $this->bookable = $this->instance->bookable;
        }
    }

    public function render()
    {
        $this->grid             = $this->genGridArray();
        $this->template->grid   = $this->grid;
        $this->template->setFile(__DIR__ . '\default.latte');
        $this->template->render();
    }

    /**
     * Generate an empty grid
     * @return ArrayHash
     */
    public function genGridArray()
    {
        $out = new ArrayHash;

        for($row = 0; $row < $this->row; $row++){
            $out[$row] = new ArrayHash;
            for($column = 0; $column < $this->col; $column++){
                $id             = $this->getId($column,$row); // column = X; row = Y;
                $data           = new ArrayHash();
                $data->active   = $this->isBookable($id);

                $out[$row][$id] = $data;
            }
        }
        return $out;
    }

    /**
     * Get X Y coordinates of cell ID
     * @param $id
     * @return ArrayHash
     */
    public function getCoordinates($id)
    {
        $out        = new ArrayHash;
        $out->id    = $id;
        $out->x     = ($id % $this->col);
        $out->y     = (int)($id / $this->col);
        return $out;
    }

    /**
     * Get cell ID from coordinates
     * @param int $x
     * @param int $y
     * @return int
     */
    public function getId($x, $y)
    {
        return (int) ($x + $this->col * $y);
    }

    public function handleSize($x = 1, $y = 1)
    {
        $this->col  = $x;
        $this->row  = $y;
        $this->grid = $this->genGridArray();
        $this->redrawControl('grid');
    }

    /**
     * Check if provided ID is in DB marked as bookable
     * @param $id
     * @return bool
     */
    protected function isBookable($id)
    {
        if($this->bookable){
            foreach($this->bookable as $place){
                if($id == $place->seat){
                    return TRUE;
                }
            }
            return FALSE;
        }else{
            return FALSE;
        }



    }
}