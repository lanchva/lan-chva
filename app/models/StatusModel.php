<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 4.5.2016
 * Time: 21:43
 */

namespace App\Models;


class StatusModel extends BaseModel
{
    /**
     * @param string $entity
     * @param string $name
     * @return static
     */
    public function getStatus($entity, $name)
    {
        return $this->status()->where([
            'entity'    => $entity,
            'name'      => $name
        ]);
    }
}