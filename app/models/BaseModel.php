<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 3.5.2016
 * Time: 19:28
 */

namespace App\Models;

use Nette\Database\Context;
use Nette\Object;
use Nette\Security\User;

abstract class BaseModel extends Object
{
    /** @var Context Database main connector */
    protected $db;

    /**
     * BaseModel constructor.
     * @param Context $db
     */
    public function __construct(Context $db)
    {
        $this->db = $db;
    }

    /**
     * Object with user table
     * @return \Nette\Database\Table\Selection
     */
    public function user()
    {
        return $this->db->table('user');
    }

    /**
     * Object with status table
     * @return \Nette\Database\Table\Selection
     */
    public function status()
    {
        return $this->db->table('status');
    }

    /**
     * Object with location table
     * @return \Nette\Database\Table\Selection
     */
    public function location()
    {
        return $this->db->table('location');
    }

    /**
     * Object with status table
     * @return \Nette\Database\Table\Selection
     */
    public function locationSeats()
    {
        return $this->db->table('location_bookable');
    }
}