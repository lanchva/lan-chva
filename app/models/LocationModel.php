<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 19.6.2016
 * Time: 19:08
 */

namespace App\Models;


use Nette\Database\Table\ActiveRow;
use Nette\Utils\ArrayHash;

class LocationModel extends BaseModel
{
    /**
     * @param ArrayHash $data
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function insert(ArrayHash $data)
    {
        return $this->location()->insert($data);
    }

    public function update(ActiveRow $row, $vals)
    {
        return $row->update($vals);
    }
}