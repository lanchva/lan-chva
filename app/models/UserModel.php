<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 3.5.2016
 * Time: 19:37
 */

namespace App\Models;

use Nette\Database\Table\ActiveRow;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

class UserModel extends BaseModel
{
    /**
     * @param $id
     * @return \Nette\Database\Table\IRow
     */
    public function getByID($id)
    {
        return $this->user()->get();
    }

    /**
     * @param array $array
     * @return static
     */
    public function getBy(array $array)
    {
        return $this->user()->where($array);
    }

    /**
     * Create new user
     * @param ArrayHash $vals
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function insert(ArrayHash $vals)
    {
        $vals->commString   = $this->newCommString($vals->email);
        $vals->password     = $this->getHash($vals->password);

        return $this->user()->insert($vals);
    }

    /**
     * Get hashed string from string
     * @param $string
     * @return string
     */
    public function getHash($string)
    {
        return Passwords::hash($string);
    }

    /**
     * @param $email
     * @return string
     */
    public function newCommString($email)
    {
        $date   = new DateTime();
        $string = $email . $date;
        $hash   = $this->getHash($string);
        return Strings::webalize($hash);
    }

    /**
     * @param ActiveRow $user
     * @param ActiveRow $newStatus
     * @return bool
     */
    public function changeStatus(ActiveRow $user, ActiveRow $newStatus)
    {
        return $user->update(['status' => $newStatus]);
    }
}