<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 4.5.2016
 * Time: 20:54
 */

namespace App\Models\Services;

use App\Models\StatusModel;
use App\Models\UserModel;
use Exception;
use MongoDB\Driver\Exception\DuplicateKeyException;
use Nette\Database\Table\ActiveRow;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;


class userService extends \Nette\Object implements IAuthenticator
{
    /** @var UserModel $model */
    private $userModel;

    /** @var StatusModel $statusModel */
    private $statusModel;

    public function __construct(UserModel $user, StatusModel $status)
    {
        $this->userModel    = $user;
        $this->statusModel  = $status;
    }

    /**
     * @param array $credentials
     * @return IIdentity
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        $username = $credentials[0]->nick;
        $password = $credentials[0]->password;

        /** @var ActiveRow $user */
        $user = $this->userModel->user()->where(['nick' => $username])
            ->fetch();

        if(!$user){
            throw new AuthenticationException('Neznámé uživatelské jméno nebo špatné heslo heslo', self::IDENTITY_NOT_FOUND);
        }elseif (!Passwords::verify($password, $user->password)) {
            throw new AuthenticationException('Neznámé uživatelské jméno nebo špatné heslo heslo', self::INVALID_CREDENTIAL);
        }elseif ($user->status !== 2){
            throw new AuthenticationException('Neaktivní uživatel', self::NOT_APPROVED);
        }elseif (Passwords::needsRehash($user->password)){
            $user->update(['password' => Passwords::hash($password)]);
        }else{
            $arr = $user->toArray();
            unset($arr['password']);
            return new Identity($user->id, $user->role, $arr);
        };
    }

    /**
     * @param ArrayHash $values
     * @return bool|int|\Nette\Database\Table\IRow
     * @throws Exception
     */
    public function add(ArrayHash $values)
    {
        try{
            $newUser = $this->userModel->insert($values);
        }catch (UniqueConstraintViolationException $e){
            throw new UniqueConstraintViolationException;
        }

        return $newUser;
    }

    public function verification(ActiveRow $user)
    {
        $newStatus = $this->statusModel->getStatus('user', 'active')
            ->fetch();
        $user->update(['commString' => NULL]);
        $this->userModel->changeStatus($user, $newStatus);
    }
}