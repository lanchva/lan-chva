<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 19.6.2016
 * Time: 19:07
 */

namespace App\Models\Services;

use App\Models\LocationModel;
use Nette\Database\Table\ActiveRow;
use Nette\Object;
use Nette\Utils\ArrayHash;

class locationService extends Object
{
    /** @var LocationModel $locationModel */
    public $locationModel;

    public function __construct(LocationModel $locationModel)
    {
        $this->locationModel = $locationModel;
    }

    /**
     * @param $metaData
     * @param $seats
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function handle($metaData, $seats)
    {
        if($metaData->id){
            $location   = $this->locationModel->location()->get($metaData->id);
            $result     = $this->edit($location, $metaData);
            $seats      = $this->setSeats($location, $seats);
        }else{
            $result     = $this->add($metaData);
            $seats      = $this->setSeats($result, $seats);
        }

        return $result;
    }

    /**
     * @param ArrayHash $metaData
     * @return bool|int|\Nette\Database\Table\IRow
     * ToDo: Exception on duplication
     */
    private function add (ArrayHash $metaData)
    {
        try{
            return $this->locationModel->insert($metaData);
        }catch (\Exception $e){
            return $e;
        }
    }

    /**
     * @param ActiveRow $location
     * @param $data
     * @return bool
     */
    private function edit(ActiveRow $location, $data)
    {
        unset($data->id, $data->createdBy_id);
        return $this->locationModel->update($location, $data);
    }

    /**
     * @param ActiveRow $metaData
     * @param array $seats
     * @return bool|int|\Nette\Database\Table\IRow
     */
    private function setSeats(ActiveRow $metaData, array $seats)
    {
        $out    = [];
        foreach ($seats as $seat){
            $one                = new ArrayHash();
            $one->location_id   = $metaData->id;
            $one->seat          = $seat;
            $out[]              = $one;
        }

        $allocatedSeats = $this->locationModel->locationSeats()
            ->where(['location_id' => $metaData->id]);
        $allocatedSeats->delete();

        return $this->locationModel->locationSeats()->insert($out);
    }

    /**
     * Get available seats for specific location
     * @param ActiveRow $location
     * @return \Nette\Database\Table\GroupedSelection
     */
    public function getSeats(ActiveRow $location)
    {
        return $location->related('location_bookable.location_id');
    }
}