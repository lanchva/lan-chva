<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 3.5.2016
 * Time: 20:07
 */

namespace App\Presenters;


use Nette\Application\UI\Presenter;
use Nette\Reflection\ClassType;
use Nette\Utils\Strings;
use Tracy\Debugger;
use WebLoader\Nette\LoaderFactory;

class BasePresenter extends Presenter
{
    /** @var LoaderFactory WebLoader */
    protected $webLaoder;

    /** @var string WebLoader Template */
    protected $webLoaderTemplate = 'default';

    /**
     * @param LoaderFactory $factory
     */
    public function injectWebLoader(LoaderFactory $factory)
    {
        $this->webLaoder = $factory;
    }

    protected function createComponentCss()
    {
        return $this->webLaoder->createCssLoader($this->webLoaderTemplate);
    }

    protected function createComponentJs()
    {
        return $this->webLaoder->createJavaScriptLoader($this->webLoaderTemplate);
    }

    public function getModulePrefix()
    {
        $pos = strrpos($this->name, ':');
        if (is_int($pos)) {
            $name = substr($this->name, 0, $pos);
            return Strings::lower($name);
        }

        return '';
    }
}