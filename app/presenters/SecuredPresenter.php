<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 3.5.2016
 * Time: 20:55
 */

namespace App\Presenters;


class SecuredPresenter extends BasePresenter
{
    public function beforeRender()
    {
        if(!$this->user->isLoggedIn())
        {
            $this->flashMessage("Přihlašte se prosím", 'danger');
            $this->redirect('Login:');
        }
    }
}