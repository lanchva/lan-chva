<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 4.5.2016
 * Time: 20:19
 */

namespace App\FrontModule\Presenters;


use App\Models\Services\userService;
use App\Models\UserModel;
use Components\Forms\IRegistration;

class RegistrationPresenter extends BasePresenter
{
    /** @var UserModel @inject */
    public $userModel;

    /** @var userService @inject */
    public $userService;
    
    /** @var IRegistration @inject */
    public $regForm;

    public function renderVerification($id)
    {
        if($id){
            $user = $this->userModel->getBy(['commString' => $id])->fetch();
            if($user){
                $this->userService->verification($user);
                $this->flashMessage('Verifikace emailu proběhla úspěšně, nyní se můžete přihásit');
                $this->redirect('Login:');
            }else{
                $this->flashMessage('Neznámý uživatel!', 'danger');
                $this->redirect('Homepage:');
            }
        }else{
            $this->redirect('Homepage:');
        }
    }
    
    
    protected function createComponentRegistration()
    {
        $control = $this->regForm->create();
        $control->onSave[] = function(){
            $this->flashMessage('Registrace proběhla úspěšně, zkontrolujte prosím svůj email', 'success');
            $this->redirect('Homepage:');
        };

        return $control;
    }
}