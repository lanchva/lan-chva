<?php

namespace App\FrontModule\Presenters;


use App\Models\LocationModel;
use Components\Forms\ILocations;
use Nette;


class HomepagePresenter extends BasePresenter
{
    /** @var ILocations @inject */
    public $form;

    /** @var LocationModel @inject */
    public $locationModel;

    /** @var  Nette\Database\Table\ActiveRow $location */
    protected $location;

    public function renderDefault($id = null)
    {
        $location = $this->locationModel->location()->get($id);
        if($location){
            $this->location = $location;

        }else{
            $this->flashMessage('Neznámá lokalita');
            //$this->redirect('Homepage:default');
        }
    }

    protected function createComponentGrid()
    {
        return $this->form->create($this->location);
    }
}