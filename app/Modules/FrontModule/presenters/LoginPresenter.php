<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 3.5.2016
 * Time: 20:57
 */

namespace App\FrontModule\Presenters;


use Components\Forms\ILogin;

class LoginPresenter extends BasePresenter
{
    /** @var ILogin @inject */
    public $loginForm;

    public function createComponentLogin()
    {
        $control = $this->loginForm->create();
        $control->onSave[] = function(){
            $this->flashMessage('Příhlášení proběhlo úspěšně', 'success');
            $this->redirect('Homepage:');
        };
        return $control;
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Uživatel úspěšně odhlíšen', 'success');
        $this->redirect('Homepage:');
    }
}