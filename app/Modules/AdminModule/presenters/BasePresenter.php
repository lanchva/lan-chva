<?php
/**
 * Created by PhpStorm.
 * User: olang
 * Date: 27.5.2016
 * Time: 18:47
 */

namespace App\AdminModule\Presenters;

use \App\Presenters as Root;

abstract class BasePresenter extends Root\BasePresenter
{
    public function beforeRender()
    {
        parent::beforeRender();
        $user = $this->getUser();
        if(!$user->isInRole('admin')){
            $this->flashMessage('Na toto nemáte oprávnění!', 'danger');
            $this->redirect(':Front:Homepage:default');
        }
    }
}