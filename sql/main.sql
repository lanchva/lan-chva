-- --------------------------------------------------------
-- Hostitel:                     127.0.0.1
-- Verze serveru:                10.1.8-MariaDB - mariadb.org binary distribution
-- OS serveru:                   Win32
-- HeidiSQL Verze:               9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportování struktury databáze pro
CREATE DATABASE IF NOT EXISTS `lanchva` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci */;
USE `lanchva`;


-- Exportování struktury pro tabulka lanchva.location
CREATE TABLE IF NOT EXISTS `location` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `street` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `number` int(11) NOT NULL,
  `cols` int(11) NOT NULL DEFAULT '1',
  `rows` int(11) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdBy_id` int(32) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_location_user` (`createdBy_id`),
  CONSTRAINT `FK_location_user` FOREIGN KEY (`createdBy_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='table which contain metadata of lan-parties';

-- Exportování dat pro tabulku lanchva.location: ~0 rows (přibližně)
DELETE FROM `location`;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
/*!40000 ALTER TABLE `location` ENABLE KEYS */;


-- Exportování struktury pro tabulka lanchva.location_bookable
CREATE TABLE IF NOT EXISTS `location_bookable` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `location_id` int(32) NOT NULL,
  `seat` int(32) NOT NULL COMMENT 'ID of bookable seat',
  PRIMARY KEY (`id`),
  UNIQUE KEY `location_id_table_id` (`location_id`,`seat`),
  CONSTRAINT `FK_location_bookabe_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='list of available tables';

-- Exportování dat pro tabulku lanchva.location_bookable: ~0 rows (přibližně)
DELETE FROM `location_bookable`;
/*!40000 ALTER TABLE `location_bookable` DISABLE KEYS */;
/*!40000 ALTER TABLE `location_bookable` ENABLE KEYS */;


-- Exportování struktury pro tabulka lanchva.status
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `entity` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku lanchva.status: ~4 rows (přibližně)
DELETE FROM `status`;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`id`, `name`, `entity`, `description`) VALUES
	(1, 'new', 'user', 'Nový uživatel, je potřeba potvrdit email'),
	(2, 'active', 'user', 'Aktivní uživatel'),
	(3, 'deactive', 'user', 'Užival se nemůže zalogovat'),
	(4, 'ban', 'user', 'Uživatel se z poslední IP nedostane na stránky');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;


-- Exportování struktury pro tabulka lanchva.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `nick` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `surname` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `birthdate` datetime DEFAULT NULL,
  `role` enum('user','admin') COLLATE utf8_czech_ci NOT NULL DEFAULT 'user',
  `terms` tinyint(1) DEFAULT NULL,
  `termsAcceptedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastLoginDate` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `commString` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `exp` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nick` (`nick`),
  KEY `FK_user_status` (`status`),
  CONSTRAINT `FK_user_status` FOREIGN KEY (`status`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku lanchva.user: ~0 rows (přibližně)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
