Language: English

Interpreter: PHP

Framework: [Nette](https://nette.org/)

Pre-requirements:

1. WAMP/LAMP

    * Software stack of Apache, MySQL (MariaDB), PHP and CURL (openSSL included)
    * For windows users - [XAMPP](https://www.apachefriends.org/index.html) is preferred

2. [Composer](https://getcomposer.org/) - Dependency Manager
3. [GitBash](https://github.com/msysgit/msysgit/releases/) - for windowns users